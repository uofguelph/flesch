--------------------------------------------------------------
-- Program: Flesch              Date: April 6, 2018         --
-- Author: Dhruv Lad                 Student ID: 0928018    --
-- Class: CIS*3190                  Assignment: A4          --
--             											    --
--------------------------------------------------------------

with Ada.Text_IO;use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Unbounded.Text_IO; use Ada.Strings.Unbounded.Text_IO;
with Ada.Characters.Handling; use Ada.Characters.Handling;


procedure flesch is
    -- delcaring variable name its types
    File: Ada.Text_IO.File_Type;
    lineIn:Unbounded_String;
    choice: String(1..2);
    userSentence : Unbounded_String;
    cLen :Natural;
    file_line: String(1..15);
    Last: Natural;
    Count: Natural:=0;
    word: Natural:=0;
    vCount: Natural:=0;
    index: Float:=0.00;
    solone: Float:=0.00;
    soltwo: Float:=0.00;
    wordpersen: Float := 0.00;
    syllperword : Float:=0.00;
    peek: Natural:=0;
    iIndex:Natural:=0;
begin
    --asking user if they inputing file name or a sentence
    Put_Line ("1) to Enter a File Name, 2) to Enter the Sentence");
    Get_Line(choice,cLen);
    
    --if the choice they picked was file name then do this 
    if(choice(1..cLen) = "1") then
    Put_Line("Enter a filename: ");
    --get the file name
    Get_Line(file_line,Last);
    --opens the file
    open(File,in_file,file_line(1..Last));

  --  loop until the end of file
  while not Ada.Text_IO.End_Of_File(File) loop
        --get the line from the file
        get_line(File,lineIn);
        for i in 1..Length(lineIn) loop
        --check to see if there are syllablas in the sentence and no letter after that
            if((Element(lineIn,i))='a') or ((Element(lineIn,i))='e') or ((Element(lineIn,i))='i') or ((Element(lineIn,i))='o') or ((Element(lineIn,i))='u') or ((Element(lineIn,i))='y') then
                vCount:=vCount+1;

                peek:=i+2;
                --check to see if the character are e and s and no letter after that
                if (peek <= Length(lineIn)) then
                    if((Element(lineIn,i))='e') and ((Element(lineIn,i+1))='s') and not Is_Letter(Element(lineIn,i+2)) then
                        vCount:=vCount-1;
                    end if;
                end if;
                peek:=i+2;
                --check to see if the character are e and d and no letter after that
                if (peek <= Length(lineIn)) then
                    if((Element(lineIn,i))='e') and ((Element(lineIn,i+1))='d') and not Is_Letter(Element(lineIn,i+2)) then
                        vCount:=vCount-1;
                    end if;
                end if;
                peek:=i+1;
                --check to see if the character are e and no letter after that
                if (peek <= Length(lineIn)) then
                    if((Element(lineIn,i))='e') and not Is_Letter(Element(lineIn,i+1)) then
                        vCount:=vCount-1;
                    end if;
                end if;
                peek:=i+1;
                --checks for the doubles of the words
                if (peek <= Length(lineIn)) then
                    if((Element(lineIn,peek))='a') or ((Element(lineIn,peek))='e') or ((Element(lineIn,peek))='i') or ((Element(lineIn,peek))='o') or ((Element(lineIn,peek))='u') or ((Element(lineIn,peek))='y') then
                        vCount:=vCount-1;
                    end if;
                end if;
	            --check to see if the character is space
                elsif((Element(lineIn,i))=' ') then
                    word:=word+1;

                    if((Element(lineIn,i+1))=' ')  then
                        word:=word-1;
                    end if;

                    for j in 1..3 loop
                        if((Element(lineIn,j))=' ') then
                            vCount:=vCount+1;
                        end if;
                    end loop;
                    elsif((Element(lineIn,i))='.') or ((Element(lineIn,i))='!') or ((Element(lineIn,i))='?') or ((Element(lineIn,i))=':') or ((Element(lineIn,i))=';') then
                        Count:=Count+1;
                    end if;
                end loop;
            end loop;
            word:=word+1;
            New_Line;
            Put("Sentences: ");
            Put(Integer'Image(Count));
            Put(" , Words: ");
            Put(Integer'Image(word));
            Put(" , Syllabals: ");
            Put(Integer'Image(vCount));
            Put(" ,Sentences per Word:");
            wordpersen := Float(word/Count);
            Put(Float'Image(wordpersen));
            Put(" , Syllabals per Word: ");
            syllperword := Float(vCount/word);
            Put(Float'Image(syllperword));
            solone:=1.015 * Float((word)/(Count));
            soltwo:=84.6 * Float((vCount)/(word));
            Put(" , Flesch Index:");
            index:=206.835 - solone - soltwo;
            iIndex:=Integer(index);
            Put(Integer'Image(iIndex));
            Put(" , Flesch-Kincaid Index");
            index:= 0.39 * (Float(word/Count));
            index:= index + (11.8 * Float(vCount/word));
            index:=index - 15.59;
            if (index < 0.00)then
            Put(Natural'Image(0));
            else
            iIndex:= Integer(index);
            Put(Integer'Image(iIndex));
            end if;
            close (File); 
             
        -- if its a sentence then do this part      
        else 
        --get the users sentence
        Put_Line("Enter a Sentence: ");
        Get_Line(userSentence);
        
        for i in 1..Length(userSentence) loop
        --check to see if there are syllablas in the sentence and no letter after that
            if((Element(userSentence,i))='a') or ((Element(userSentence,i))='e') or ((Element(userSentence,i))='i') or ((Element(userSentence,i))='o') or ((Element(userSentence,i))='u') or ((Element(userSentence,i))='y') then
                vCount:=vCount+1;

                peek:=i+2;
                --check to see if the character are e and s and no letter after that
                if (peek <= Length(userSentence)) then
                    if((Element(userSentence,i))='e') and ((Element(userSentence,i+1))='s') and not Is_Letter(Element(userSentence,i+2)) then
                        vCount:=vCount-1;
                    end if;
                end if;
                peek:=i+2;
                --check to see if the character are e and d and no letter after that
                if (peek <= Length(lineIn)) then
                    if((Element(userSentence,i))='e') and ((Element(userSentence,i+1))='d') and not Is_Letter(Element(userSentence,i+2)) then
                        vCount:=vCount-1;
                    end if;
                end if;
                peek:=i+1;
                --check to see if the character is e and next character not letter 
                if (peek <= Length(userSentence)) then
                    if((Element(userSentence,i))='e') and not Is_Letter(Element(userSentence,i+1)) then
                        vCount:=vCount-1;
                    end if;
                end if;
                peek:=i+1;
                if (peek <= Length(userSentence)) then
                --checks for the doubles of the words
                    if((Element(userSentence,peek))='a') or ((Element(userSentence,peek))='e') or ((Element(userSentence,peek))='i') or ((Element(userSentence,peek))='o') or ((Element(userSentence,peek))='u') or ((Element(userSentence,peek))='y') then
                        vCount:=vCount-1;
                    end if;
                end if;

                elsif((Element(userSentence,i))=' ') then
                    word:=word+1;

                    if((Element(userSentence,i+1))=' ')  then
                        word:=word-1;
                    end if;

                    for j in 1..3 loop
                        if((Element(userSentence,j))=' ') then
                            vCount:=vCount+1;
                        end if;
                    end loop;
                    elsif((Element(userSentence,i))='.') or ((Element(userSentence,i))='!') or ((Element(userSentence,i))='?') or ((Element(userSentence,i))=':') or ((Element(userSentence,i))=';') then
                        Count:=Count+1;
                    end if;
                end loop;
            word:=word+1;
            
            --calcualting the sentence, word, syllabals, flesch Index and fles-Kincaid Index
            New_Line;
            Put("Sentences: ");
            Put(Integer'Image(Count));
            Put(" ,Words: ");
            Put(Integer'Image(word));
            Put(" ,Syllabals: ");
            Put(Integer'Image(vCount));
            Put(" ,Sentences per Word:");
            wordpersen := Float(word/Count);
            Put(Float'Image(wordpersen));
            Put(" , Syllabals per Word: ");
            syllperword := Float(vCount/word);
            Put(Float'Image(syllperword));
            solone:=1.015 * Float((word)/(Count));
            soltwo:=84.6 * Float((vCount)/(word));
            Put(" ,Flesch Index:");
            --math calcualtion to find flesch index
            index:=206.835 - solone - soltwo;
            iIndex:=Integer(index);
            Put(Integer'Image(iIndex));
            Put(" ,Flesch-Kincaid Index");
            --math calculation to find flesch-kincaid index
            index:= 0.39 * (Float(word/Count));
            index:= index + (11.8 * Float(vCount/word));
            index:=index - 15.59;
            if (index < 0.00)then
            Put(Integer'Image(0));
            else
            iIndex:= Integer(index);
            Put(Integer'Image(iIndex));
            end if;
        end if;
        
        end;

